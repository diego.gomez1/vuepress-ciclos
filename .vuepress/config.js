const { config } = require("vuepress-theme-hope");

module.exports = config({
  title: "Ciclos formativos",
  locales: {
    "/": {
      lang: "es-ES",
      title: "Ciclos formativos",
      description: "Documentación de ciclos formativos de Escuela IDEO",
    },
  },
  head: [
    ["link", { rel: "icon", href: `/logo.png` }],
    ["link", { rel: "manifest", href: "/manifest.json" }],
    ["link", { rel: "canonical", href: "https://vuepress-deploy.netlify.com" }],
    ["meta", { name: "theme-color", content: "#3eaf7c" }],
    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" },
    ],
    [
      "link",
      { rel: "apple-touch-icon", href: `/icons/apple-touch-icon-152x152.png` },
    ],
    [
      "link",
      {
        rel: "mask-icon",
        href: "/icons/safari-pinned-tab.svg",
        color: "#3eaf7c",
      },
    ],
    [
      "meta",
      {
        name: "msapplication-TileImage",
        content: "/icons/msapplication-icon-144x144.png",
      },
    ],
    ["meta", { name: "msapplication-TileColor", content: "#000000" }],
  ],
  serviceWorker: true,
  themeConfig: {
    docsDir: "docs",
    repo: "capriosa/vuepress-deploy",

    nav: [
      {
        text: "TSDAM",
        link: "/tsdam/",
      },
      {
        text: "TSDAW",
        link: "/tsdaw/",
      },
      {
        text: "Empresas",
        link: "/companies/",
      },
    ],

    sidebar: {
      "/tsdam/": [
        {
          title: "Get Started",
          icon: "creative",
          collapsable: false,
          children: [
            "",
            "methodology",
            "evaluation",
            "teachers",
            "environment",
          ],
        },
        {
          title: "Módulos",
          icon: "discover",
          prefix: "subjects/",
          collapsable: true,
          children: [
            "",
            {
              title: "Curso 1",
              icon: "home",
              prefix: "course1/",
              collapsable: true,

              children: ["pr", "ssii", "eedd", "fol", "bbdd", "llmm"],
            },
            {
              title: "Curso 2",
              icon: "home",
              prefix: "course2/",
              collapsable: true,
              children: [
                "add",
                "dii",
                "psspp",
                "pmddmm",
                "ssge",
                "eie",
                "english",
                "project",
                "fcct",
              ],
            },
          ],
        },
      ],
      "/tsdaw/": [""],
      "/companies/": [""],
      "/docs/": [""],
      "/": [""],
    },
  },
});
