# Metodología

## Organización de horario

El ciclo formativo se estructura en una secuencia que favorezca el desarrollo de competencias.

### Secuenciación de la sesión de módulo

Cada módulo se estructura en función de la carga lectiva en una serie de unidades (entre 5 y 10) en la que se agrupa
de forma relacional los contenidos y destrezas a trabajar.

Cada unidad se desarrolla en relación con el resto de módulos trabajados.

### Secuenciación de la sesión de empresa

El desarrollo de empresa se centra principalmente en la adquisición de destrezas de las 4 "Cs":

- Comunicativas
- Colaborativas
- Críticas
- Creativas

**Comunicativas**: Estrategias de comunicación con el resto del equipo y del propio trabajo realizado. Más importante
que el desarrollo de un proyecto es saber difundirlo y comunicarlo. Se reforzará cuestiones como generar un documentación
útil que contribuya a visibilizar el proyecto. Desde la perspectiva del desarrollo del perfil profesional se creará
un portfolio personal que muestre todo el trabajo realizado.

**Colaborativas**: Estrategias de trabao en equipo y resolución de conflictos, valorando las destrezas de los demás y
la ventaja de contar con equipos interdisciplinares.

**Críticas**: Adquirir una visión sistémica sobre la realidad y adoptar una postura de mejora y superación de las
habilidades individuales. Destacando los puntos fuertes y trabajando por fortalecer los débiles. Adoptar una postura
participativa y que contribuya de forma activa al desarrollo de las propuestas.

**Creativas**: Adoptar una actitud investigadora que contribuya al enriquecimiento personal de la visión sobre las
opciones disponibles para afrontar un problema. Desde esta perspectiva siempre se trabajará por buscar opciones
nueves y creativas para resolver los problemas que se presenten.

## Aprendizaje basado en retos

## Evaluación

El proceso de evaluación es esencial en la propuesta metodológica del ciclo, y persigue siempre un fin formativo. En la sección [Evaluación](./evaluation.md) se
profundiza sobre este aspecto.

## Autonomía

## Herramientas de soporte metodológico

En este apartado se lista un repositorio de herramientas que pueden apoyar la docencia del ciclo.

### Plataformas para compartir proyectos

- [Gitlab](https://www.gitlab.com) -
- [Github](https://www.github.com) -
- [Glitch](https://www.glitch.com) -

### Gestión de proyectos

- [Codecks](https://www.codecks.io/) - Herramienta de gestión de proyecto basado en SCRUM.

### Retos de programación

- [Code Wars](https://www.codewars.com/) - Retos para resolver en diferentes lenguajes, organizados por categorías de dificultad. `8kyu` es la dificultad más
  baja y `1kyu` es dificultad muy alta.
- [Coding Game](https://www.codingame.com/) - Retos de programación organizados en 4 niveles (`easy`, `medium`, `hard` y `very hard`). Ofrece tanto actividades
  para practicar como competiciones clasificatorias. Los retos estás organizados por temáticas.

### Corrección automática

Los sistemas de programación con corrección automática se logran por medio de la definición de pruebas unitarias. Para soportar el desarrollo del ciclo.

### Organización de trabajo

La organización del trabajo en equipo y de las tareas.

- [Codecks](https://www.codecks.io) - Programa para secuenciar el trabajo por medio de tarjetas siguiendo la metodología SCRUM.

### Servicios de hosting

- [Netlify](https://www.netlify.com) - Servicio para el despliegue de páginas estáticas, que pueden generarse con sistemas como
  [Vuepress](https://vuepress.vuejs.org/) o [eleventy](https://www.11ty.dev/).
- [Vercel](https://www.vercel.com) -
- [Heroku](https://www.heroku.com) -
