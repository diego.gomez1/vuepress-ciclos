# Perfil docente

## Requisitos legislativos

En el el Anexo III A y Anexo III B del [Real Decreto 450/2010](https://www.boe.es/boe/dias/2010/05/20/pdfs/BOE-A-2010-8067.pdf) se especifica dos perfiles profesionales en
función de los módulos profesionales.

<table>
<tr><th style="width:50%">Módulos</th><th>Formación</th></tr>
<tr>
    <td>
    0490. Programación de servicios y procesos. <br/>
    0489. Programación multimedia y dispositivos móviles. <br/>
    0491. Acceso a datos. <br/>
    0487. Entornos de desarrollo. <br/>
    0373. Lenguajes de marcas y sistemas de gestión de información. <br/>
    0492. Programación. <br/>
    0493. Bases de Datos. <br/>
    0494. Formación y orientación laboral. <br/>
    0495. Empresa e iniciativa emprendedora. <br/>
    </td>
    <td>
    <ul>
    <li>Licenciado, Ingeniero, Arquitecto o el título de
    grado correspondiente u otros títulos equivalentes
    a efectos de docencia.</li>
    <li>Titulación a efectos de <b>docencia</b> correspondiente a la rama (Diplomado en Estadística;
Ingeniero Técnico en Informática de Gestión;
Ingeniero Técnico en Informática de Sistemas;
Ingeniero Técnico de Telecomunicación,especialidad en Telemática.)</li>
</ul>
    </td>
</tr>
<tr>
    <td>
    0492. Proyecto de desarrollo de aplicaciones multiplataforma.<br/>
    0491.Sistemas de gestión empresarial.<br/>
    0488. Desarrollo de interfaces.<br/>
    0483.Sistemas informáticos.<br/>
    </td>
    <td>
    <ul>
    <li>Licenciado, Ingeniero, Arquitecto o el título de
    grado correspondiente u otros títulos equivalentes
    a efectos de docencia.</li>
    <li>Diplomado, Ingeniero Técnico o Arquitecto
    Técnico o el título de grado correspondiente u
    otros títulos equivalentes.</li>
    <li>Titulación a efectos de <b>docencia</b> correspondiente a la rama (Diplomado en Estadística;
    Ingeniero Técnico en Informática de Gestión;
    Ingeniero Técnico en Informática de Sistemas;
    Ingeniero Técnico de Telecomunicación,especialidad en Telemática.)</li>
    </ul>
    </td>
</tr>
<tr>
    <td>
    0493. Formación y orientación laboral<br/>
    0494. Empresa e iniciativa emprendedora.<br/>    
    </td>
    <td>
    <ul>    
    <li>Titulación a efectos de <b>docencia</b> correspondiente a la rama:
    <ul>
        <li>Diplomado en Ciencias Empresariales.</li>
        <li>Diplomado en Relaciones Laborales.</li>
        <li>Diplomado en Trabajo Social.</li>
        <li>Diplomado en Educación Social.</li>
        <li>Diplomado en Gestión y Administración Pública</li>
    </ul>
    </li>
    </ul>
    </td>
</tr>
</table>

## Perfil docente deseado

En cumplimiento con la legislación vigente y el rol del docente las características deseadas del profesorado son:

- Licenciado, Ingeniero, Arquitecto o el título de grado correspondiente u otros títulos equivalentes a efectos de docencia.
- Titulación de Máster de Secundaria de la rama Ingeniero Técnico en Informática de Gestión; Ingeniero Técnico en Informática de Sistemas; Ingeniero Técnico de Telecomunicación,especialidad en Telemática.

Es deseable que el profesorado cuente con experiencia de trabajo en la empresa, para entender y trasmitir sus dinámicas.

También se recomienda que tenga un perfil de competencia alto en las destrezas que se trabajan en los ciclos, de forma que incluya:

- Desarrollo de aplicaciones
- Uso de sistemas informáticos
- Integración entre sistemas
- Uso de metodologías ágiles

Dado el perfil transversal y aplicado de las materias donde se requiere un perfil de Formación y Orientación Laboral, este perfil
debe complementarse desde el docente del resto de módulos y con colaboración con el departamento de orientación de la escuela.

## Rol del docente

El docente es un guía que debe orientar en el desarrollo de las destrezas y competencias profesionales.

Entre las tareas del docente identificamos:

- Secuenciación de los módulos.
- Generación o elección de experiencias de aprendizaje.
- Dinamización de los equipos para favorecer el aprendizaje entre iguales y autónomo.
- Evaluación de los procesos de aprendizaje.

Según el espacio de aprendizaje planteamos 3 perfiles o roles que asume el docente.

### Presentación de contenidos de módulos

Selecciona las estrategias y materiales para introducir los contenidos a trabajar en cada uno de los bloques o unidades de los módulos.
Se ha de intentar que exista una correlación entre los bloques trabajados, de forma que los aprendizajes se vayan construyendo de
forma contextualizada.

### Aprendizaje por retos

Elección de propuestas de aprendizaje autónomo, donde se profundice en los aprendizajes presentados. Se debe de favorecer tanto la mentoría entre
estudiantes como la evaluación y corrección cruzada.

### Espacio de empresa

En este espacio se trabajan proyectos interdisciplinares y con aplicación real, a la par que se trabajan aspectos transversales como las metodologías
de desarrollo y las dinámicas de los equipos.

Se han de seleccionar proyectos cuyo nivel de dificultad sea asumible por el equipo, pero que puede ir más allá de los contenidos abordados
en los módulos.

Durante el trabajo de los equipos, el profesorado asesora, acompaña y reconduce el trabajo para asegurar que los proyectos continúan en desarrollo
y los equipos van adquiriendo de forma progresiva estrategias de autogestión.
