# Programación multimedia y dispositivos móviles

- Denominación: **Programación multimedia y dispositivos móviles**
- Horas: **80**
- Curso: **2º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
