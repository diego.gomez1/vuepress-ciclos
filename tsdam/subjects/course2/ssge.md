# Sistemas de gestión empresarial

- Denominación: **Sistemas de gestión empresarial**
- Horas: **95**
- Curso: **2º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
