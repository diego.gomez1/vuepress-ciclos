# Empresa e iniciativa emprendedora

- Denominación: **Empresa e iniciativa emprendedora**
- Horas: **65**
- Curso: **2º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
