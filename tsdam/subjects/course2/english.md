# Inglés técnico para grado superior

- Denominación: **Inglés técnico para grado superior**
- Horas: **40**
- Curso: **2º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
