# Formación en centros de trabajo

- Denominación: **Formación en centros de trabajo**
- Horas: **370**
- Curso: **2º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
