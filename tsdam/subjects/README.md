---
---

# Módulos

A continuación se describen los diferentes módulos del ciclo.

|                    Curso 1                     |                                 Curso 2                                 |
| :--------------------------------------------: | :---------------------------------------------------------------------: |
|           [Programación](course1/pr)           |                      [Acceso a datos](course2/add)                      |
|     [Sistemas informáticos](course1/ssii)      |                 [Desarrollo de interfaces](course2/dii)                 |
|     [Entornos de desarrollo](course1/eedd)     |          [Programación de servicios y procesos](course2/psspp)          |
| [Formación y orientación laboral](course1/fol) |    [Programación multimedia y dispositivos móviles](course2/pmddmm)     |
|         [Bases de datos](course1/bbdd)         |             [Sistemas de gestión empresarial](course2/ssge)             |
|       [Lenguajes de marca](course1/llmm)       |            [Empresa e iniciativa emprendedora](course2/eie)             |
|                                                |         [Inglés técnico para grado superior\*](course2/english)         |
|                                                | [Proyecto de desarrollo de aplicación multiplataforma](course2/project) |
|                                                |             [Formación en centros de trabajo](course2/fcct)             |

\* El inglés se ofrece de forma transversal y no tiene un módulo específico. En la guía se dan indicaciones metodológicas generales, que aplican en los dos cursos.
