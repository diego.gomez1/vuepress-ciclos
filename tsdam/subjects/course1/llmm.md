# Lenguajes de marca

- Denominación: **Lenguaje de marcas y sistemas de gestión de información**
- Horas: **140**
- Curso: **1º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
