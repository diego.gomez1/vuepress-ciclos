# Programación

- Denominación: **Programación**
- Horas: **270**
- Curso: **1º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Secuenciación

Los bloques de contenido deben organizarse con sentido en torno al desarrollo del resto de módulos.

A continuación se muestra un gráfico con una posible estructuración.

![Secuencia de programación](./assets/pr.seq.drawio.svg)

## Unidades didácticas

Los bloques de contenido se agrupan en unidades que se relacionan con los de otros módulos.

### Unidad 1 - Introducción a la programación

Bloque 1

### Unidad 2 - Introducción a la programación

Bloque 1

### Unidad 3 - Introducción a la programación

Bloque 1

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
