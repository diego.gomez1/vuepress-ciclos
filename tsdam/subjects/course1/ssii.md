# Sistemas informáticos

- Denominación: **Sistemas informáticos**
- Horas: **205**
- Curso: **1º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

El conocimiento de los sistemas operativos es fundamental para el desarrollo de aplicaciones de cliente, servidor e IOT.

A lo largo del curso se pretende dar las nociones fundamentales de los elementos principales de los sistemas operativos pero se promueve sobre todo su uso.

Otro de los elementos a dominar es el uso de la virtualización y los contenedores para el desarrollo y despliegue de sistemas. Para esto nos vamos a apollar en servidores de internet y en sistemas opensource en local y en servidores internos.

## Evaluación

## Temporalización

El módulo se imparte en el primer semestre del curso, para establecer las bases necesarias para el resto del ciclo.

## Herramientas de soporte al aprendizaje

- Docker y docker-compose
- Servidor de virtualización Proxmox.

## Relación con otros módulos

## Aporte al área de empresa

Cada equipo de trabajo dispondrá de un vpp en digital ocean con el fin de poder desplegar los proyectos públicos del equipo. El mantenimiento recaerá principalmente en uno de los miembros del equipo pero será utilizado por todos.

## Bibliografía de interés
