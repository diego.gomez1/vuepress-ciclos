# Entornos de desarrollo

- Denominación: **Entornos de desarrollo**
- Horas: **90**
- Curso: **1º**

## Objetivos

### Objetivos generales

### Objetivos específicos

## Principios metodológicos

## Evaluación

## Temporalización

## Herramientas de soporte al aprendizaje

## Relación con otros módulos

## Aporte al área de empresa
