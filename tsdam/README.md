---
title: TSDAM
lang: es-ES
---

# Técnico superior en desarrollo de aplicaciones multiplataforma

## Estructura del documento

Algunos de los elementos que se tratan en este documento son:

- [Metodología](./methodology.md): Líneas metodológicas características del ciclo formativo en Escuela IDEO.
- [Evaluación](./evaluation.md): Pautas comunes del proceso de evaluación de los diferentes módulos.
- [Perfil docente](./teachers.md): Definición del rol docente y su perfil.
- [Entorno de trabajo](./environment.md): Descripción de los elementos del aula de trabajo del ciclo formativo.

## Legislación vigente

El presente módulo se estructura en base al DECRETO 3/2011, de 13 de enero, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del ciclo formativo de grado superior correspondiente al título de Técnico Superior en Desarrollo de Aplicaciones Multiplataforma [Enlace al decreto](http://gestiona.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=7037)

Real Decreto 450/2010, de 16 de abril, por el que se establece el título de Técnico Superior en Desarrollo de Aplicaciones Multiplataforma y se fijan sus
enseñanzas mínimas.[Enlace Real Decreto](https://www.boe.es/boe/dias/2010/05/20/pdfs/BOE-A-2010-8067.pdf) Donde se establecen los resultados de aprendizaje y criterios de
evaluación. Así como las orientaciones pedagógicas.

Junto con La Ley Orgánica 5/2002, de 19 de junio, de las Cualificaciones y de la Formación Profesional, ordena un sistema integral de formación profesional, cualificaciones y acreditación que pueda responder con eficacia y transparencia a las demandas sociales y económicas a través de las diversas modalidades formativas. Con este fin se crea el Sistema Nacional de Cualificaciones y Formación Profesional, en cuyo marco deben orientarse las acciones formativas programadas y desarrolladas en coordinación con las políticas activas de empleo y de fomento de la libre circulación de los trabajadores. [Enlace a la Ley](https://www.boe.es/buscar/doc.php?id=BOE-A-2007-92)

Por conveniencia se ha organizado este decreto en la herramienta OpenSalt [Enlace al decreto en OpenSalt](https://case.escuelaideo.edu.es/cftree/doc/14)

## Actualización del currículum

Partiendo de la legislación vigente, se actualizan todos los contenidos de forma que respondan a la evolución de la tecnología cubierta y las demandas de la industria.
La metodología del ciclo está muy orientada a promover las metodologías ágiles y el desarrollo de competencias adaptativas, de forma que el alumnado sea
capaz de adaptarse a nuevos equipos y tecnologías. De ahí que se busque el enfoque práctico y aplicado de todos los aprendizajes, así como la flexibilidad
al poder adecuar los contenidos a los puntos de interés y desarrollo profesional de cada alumno. Todo esto responde al Artículo 5 del Real Decreto 450/2010, donde se
detallan las competencias profesionales, personales y sociales.
