# Evaluación

Algunos criterios a tener en cuenta en la evaluación son:

- La evaluación del ciclo busca ser formativa.
- El desarrollo competencial está por encima de los contenidos. La evaluación se centra por tanto en las competencias.
- Evaluación del 60% de los módulos con las actividades propias y un 40% los resultados del ciclo de empresa.

## Objetivo general del ciclo

::: details Enfoque legislativo
El ciclo formativo tiene el siguiente objetivo general:

> La competencia general de este título consiste en desarrollar, implantar, documentar y mantener aplicaciones informáticas multiplataforma, utilizando tecnologías y entornos de desarrollo específicos, garantizando el acceso a los datos de forma segura y cumpliendo los criterios de «usabilidad» y calidad exigidas en los estándares establecidos.

Este objetivo no cubre muchas de las destrezas profesionales que buscan las empresas, por lo que desde IDEO ofrecemos una propuesta más rica donde se potencian las competencias.

La legislación también recoge en el Real Decreto 450/2010 las `Competencias profesionales, personales y sociales` siguientes:

::: details Competencias profesionales, personales y sociales

1. Configurar y explotar sistemas informáticos, adaptando la configuración lógica del sistema según las necesidades de uso y los criterios establecidos.
2. Aplicar técnicas y procedimientos relacionados con la seguridad en sistemas, servicios y aplicaciones, cumpliendo el plan de seguridad.
3. Gestionar bases de datos, interpretando su diseño lógico y verificando integridad, consistencia, seguridad y accesibilidad de los datos.
4. Gestionar entornos de desarrollo adaptando su configuración en cada caso para permitir el desarrollo y despliegue de aplicaciones.
5. Desarrollar aplicaciones multiplataforma con acceso a bases de datos utilizando lenguajes, librerías y herramientas adecuados a las especificaciones.
6. Desarrollar aplicaciones implementando un sistema completo de formularios e informes que permitan gestionar de forma integral la información almacenada.
7. Integrar contenidos gráficos y componentes multimedia en aplicaciones multiplataforma, empleando herramientas específicas y cumpliendo los requerimientos establecidos.
8. Desarrollar interfaces gráficos de usuario interactivos y con la usabilidad adecuada, empleando componentes visuales estándar o implementando componentes visuales específicos.
9. Participar en el desarrollo de juegos y aplicaciones en el ámbito del entretenimiento y la educación empleando técnicas, motores y entornos de desarrollo específicos.
10. Desarrollar aplicaciones para teléfonos, PDA y otros dispositivos móviles empleando técnicas y entornos de desarrollo específicos.
11. Crear ayudas generales y sensibles al contexto, empleando herramientas específicas e integrándolas en sus correspondientes aplicaciones.
12. Crear tutoriales, manuales de usuario, de instalación, de configuración y de administración, empleando herramientas específicas.
13. Empaquetar aplicaciones para su distribución preparando paquetes auto instalables con asistentes incorporados.
14. Desarrollar aplicaciones multiproceso y multihilo empleando librerías y técnicas de programación específicas.
15. Desarrollar aplicaciones capaces de ofrecer servicios en red empleando mecanismos de comunicación.
16. Participar en la implantación de sistemas ERP-CRM evaluando la utilidad de cada uno de sus módulos.
17. Gestionar la información almacenada en sistemas ERP-CRM garantizando su integridad.
18. Desarrollar componentes personalizados para un sistema ERP-CRM atendiendo a los requerimientos.
19. Realizar planes de pruebas verificando el funcionamiento de los componentes software desarrollados, según las especificaciones.
20. Desplegar y distribuir aplicaciones en distintos ámbitos de implantación verificando su comportamiento y realizando las modificaciones necesarias.
21. Establecer vías eficaces de relación profesional y comunicación con sus superiores, compañeros y subordinados, respetando la autonomía y competencias de las distintas personas.
22. Liderar situaciones colectivas que se puedan producir, mediando en conflictos personales y laborales, contribuyendo al establecimiento de un ambiente de trabajo agradable, actuando en todo momento de forma respetuosa y tolerante.
23. Gestionar su carrera profesional, analizando las oportunidades de empleo, autoempleo y de aprendizaje.
24. Mantener el espíritu de innovación y actualización en el ámbito de su trabajo para adaptarse a los cambios tecnológicos y organizativos de su entorno profesional.
25. Crear y gestionar una pequeña empresa, realizando un estudio de viabilidad de productos, de planificación de la producción y de comercialización.
26. Participar de forma activa en la vida económica, social y cultural, con una actitud crítica y responsable.

:::

En el Artículo 9 se concretan aún más estos contenidos. Pero el enfoque es sobre todo aplicado al desarrollo de destrezas técnicas.

En busca de mejorar la empleabilidad del alumnado se

## Competencias transversales

En base a las características de los ciclos y los perfiles buscados en
empresa, así como teniendo en cuenta el proyecto educativo de centro, se
han definido 8 competencias fundamentales que se desarrollan durante toda
la formación y es común a la familia de ciclos de informática y comunicación.

::: details <b>1. Análisis de problemas y diseño de soluciones</b>
Dar respuestas a los problemas que se presentan es una característica esencial del programador.

Ha de ser capaz de generar las estrategias necesarias para afrontar los problemas y ofrecer múltiples soluciones.
Valorando la conveniencia de cada enfoque y su coste.
:::

::: details 2. Desarrollo crítico y responsable
La conciencia crítica y la integridad ética son valores profesionales y personales fundamentales. Desde la propia actividad
ofrecemos oportunidades de generar soluciones de valor y respetuosas, con la sociedad y el medio ambiente.
:::

::: details 3. Gestión activa del aprendizaje
Conocer cómo aprendemos y determinar las herramientas más efectivas nos permite tener una mayor adaptación al cambio
y seguir aprendiendo a medida que aparecen nuevas soluciones.

Durante el ciclo aprendemos diferentes tecnologías y el nivel de profundidad dependerá de las destrezas que vamos desarrollando.
:::

::: details 4. Autonomía y responsabilidad
:::

::: details 5. Documentación y generación de evidencias
:::

::: details 6. Conocimiento técnico
El ciclo ofrece una formación amplia en diferentes tecnologías que ofrecen una base para crear proyectos complejos.

Con el fin de poder ofrecer buenas soluciones es necesario manejar las herramientas con suficiente solvencia y
definir un contexto tecnológico completo con el que podamos trabajar.
:::

::: details 7. Trabajo en equipo
:::

::: details 8. Conocimiento funcional de empresa
:::

::: details 9. Gestión de proyectos
:::

## Evaluación competencial

La evaluación del ciclo se realiza de forma competencial. La metodología específica es la siguiente.

Se parte de 8 (ajustar) competencias transversales del ciclo.

1. Análisis de problemas y diseño de soluciones
2. Desarrollo crítico y responsable
3. Gestión activa del aprendizaje
4. Autonomía y responsabilidad
5. Documentación y generación de evidencias
6. Conocimiento técnico
7. Trabajo en equipo
8. Conocimiento funcional de empresa
9. Gestión de proyectos

Por cada materia se indican entre 6 y 10 (ajustar) competencias a partir de los objetivos globales del módulo.

Cada tarea tendrá un aporte de evaluación de esas competencias. Por ejemplo una tarea de programación corresponderá con una competencia general (autonomía) y otra específica (aprendizaje del lenguaje), a modo de rúbrica se ofrecerán 4 niveles de logro: No alzando, Nivel bajo, Cubierta, Superada.

El curso se supera cuando se haya alcanzado un nivel bajo en todas las competencias y un nivel medio de Cubierta.

La escala numérica de calificación es:

| Rango   | Calificación  |
| ------- | ------------- |
| 0-2     | No superado   |
| 2-2.5   | Suficiente    |
| 2.5-3.5 | Bien          |
| 3.5-4   | Sobresaliente |

## Evaluación común en el área de empresa

La evaluación de empresa trata de evaluar el crecimiento profesional de los alumnos en base a los roles que adoptan en los equipos.

El objetivo global de cada equipo es generar el producto que se les pide, documentar los resultados y generar aprendizaje en el camino.

Al comienzo de cada proyecto se identificarán:

- Roles de cada miembro.
- Retos identificados.
  - Tecnológicos
  - Gestión
- Objetivos de aprendizaje.

## Evaluación de cada módulo

Cada módulo define su evaluación definiendo:

- Definición de los estándares de cumplimiento de los objetivos competenciales.
- Procedimiento de evaluación.
