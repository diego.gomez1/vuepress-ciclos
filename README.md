---
home: true
pageClass: front
actionText: Hello VuePress →
actionLink: /docs/
features:
  - title: TSDAM
    details: Programación didáctica del grado superior de Desarrollo de aplicaciones Multiplataforma
    link: /tsdam/
  - title: TSDAW
    details: Programación didáctica del grado superior de Desarrollo de aplicaciones Web
    link: /tsdaw/
#- title: One-click Deploy
#  details: Use the ”Deploy to Netlify“ button below to create a new VuePress installation with one simple click. Get my wonderful VuePress theme for free.
#- title: Github
#  details: You need a Github and a Netlify account. Otherwise the Deploy Button doesn't work.
#- title: Netlify
#  details: The “Deploy to Netlify” button helps users deploy new sites from templates with one single click on Netlify.
---

# Programación didáctica

En esta web se describen los principales rasgos metodológicos y los criterios de evaluación de los grados superiores de **Desarrollo de aplicaciones Multiplataforma** y **Desarrollo de aplicaciones Web**.

## Más información

Se puede encontrar más información de estos ciclos en la web de la escuela:

- [Ciclo TSDAM](https://www.escuelaideo.edu.es/ciclo-formativo-tsdam/) - Información del grado superior de Desarrollo de aplicaciones **Multiplataforma**.
- [Ciclo TSDAW](https://www.escuelaideo.edu.es/ciclo-formativo-tsdaw/) - Información del grado superior de Desarrollo de aplicaciones **Web**.
