---
title: TSDAW
home: true
heroImage: /hero.png
heroText: TSDAW
tagline: Guía didáctica
actionText: Get Started →
actionLink: /guide/
features:
  - title: Simplicity First
    details: Minimal setup with markdown-centered project structure helps you focus on writing.
  - title: Vue-Powered
    details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
  - title: Performant
    details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
    footer: MIT Licensed | Copyright © 2018-present Evan You

sidebarDepth: 2
---

# Desarrollo de Aplicaciones Web

> Ciclo formativo en técnico de grado superior

## Temario

Ejemplo

### Subseccion 3

aasfdasdfa
asdfadsf

### Subseccion 3a

asdfasdf

#### Subseccion 4

asfdasdf

```
asdfasdfasd
```
